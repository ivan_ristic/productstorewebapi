﻿using ProductStore.Interfaces;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProductStore.Repository
{
    public class ProductCategoriesRepository : IDisposable, IProductCategoriesRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //getall nam vraca iz baze sve kategorije sortirane po imenu
        public IQueryable<ProductCategory> GetAll()
        {
            return db.ProductCategories.OrderBy(x => x.Name);
        }

        //getById prima parametar i na osnovu parametra pronalazi u bazi objekat i vraca
        public ProductCategory GetById(int id)
        {
            return db.ProductCategories.FirstOrDefault(x => x.Id == id);
        }

        //Add prima kao parametar objekat i dodaje u bazu
        public void Add (ProductCategory productCategory)
        {
            db.ProductCategories.Add(productCategory);
            db.SaveChanges();
        }

        //delete prima objekat i pronalazi u bazi i brise ga
        public void Delete (ProductCategory productCategory)
        {
            db.ProductCategories.Remove(productCategory);
            db.SaveChanges();
        }

        //update prima objekat, pokusamo da sacuvamo promene i ukoliko dodje do greske ispisujemo gresku u fajlu
        public void Update (ProductCategory productCategory)
        {
            db.Entry(productCategory).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
                //File.WriteAllText("../../Log/ExceptionLog.txt", "Dogodila se greska sa opisom: " + e.Message + ", molimo kontaktirajte programera");
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}