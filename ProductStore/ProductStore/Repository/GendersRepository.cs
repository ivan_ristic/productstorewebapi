﻿using ProductStore.Interfaces;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ProductStore.Repository
{
    public class GendersRepository : IDisposable, IGendresRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //getall vraca listu sortiranu po imenu
        public IQueryable<Gender> GetAll()
        {
            return db.Genders.OrderBy(x => x.Name);
        }

        //getbyid prima id i vraca nam iz beze ukoliko pronadje odgovarajuci objekat
        public Gender GetById(int id)
        {
            return db.Genders.FirstOrDefault(x=> x.Id == id);
        }

        //add prima objekat, dodaje u bazu i cuvamo promene
        public void Add(Gender gender)
        {
            db.Genders.Add(gender);
            db.SaveChanges();
        }

        //delete prima objekat, brise iz baze i cuva promenu 
        public void Delete (Gender gender)
        {
            db.Genders.Remove(gender);
            db.SaveChanges();
        }

        //update prima objekat i ukoliko dodje do greske ispisuje gresku u fajlu
        public void Update(Gender gender)
        {
            db.Entry(gender).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
                //File.WriteAllText("../../Log/ExceptionLog.txt", "Dogodila se greska sa opisom: " + e.Message + ", molimo kontaktirajte programera");
            }
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}