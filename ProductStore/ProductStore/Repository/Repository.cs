﻿using ProductStore.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ProductStore.Models;

namespace ProductStore.Repository
{
    public class Repository<TDbContext, TEntity> : IRepository<TDbContext, TEntity>
        where TDbContext : DbContext
        where TEntity : class
    {

        public IQueryable<TEntity> GetAll(TDbContext dbContext)
        {
            DbSet<TEntity> set = dbContext.Set<TEntity>();
            return set;
        }

        public TEntity GetById(TDbContext dbContext, int key)
        {
            DbSet<TEntity> set = dbContext.Set<TEntity>();
            return set.Find(key);
        }

        public void Insert(TDbContext dbContext, TEntity entity)
        {
            dbContext.Set<TEntity>().Add(entity);
        }

        public void Update(TDbContext dbContext, TEntity entity)
        {
            dbContext.Set<TEntity>().Attach(entity);
            //entity = ConvertNullable(entity);
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TDbContext dbContext, TEntity entity)
        {
            if (dbContext.Entry(entity).State == EntityState.Detached)
            {
                dbContext.Set<TEntity>().Attach(entity);
            }

            dbContext.Set<TEntity>().Remove(entity);
        }

        
    }
}