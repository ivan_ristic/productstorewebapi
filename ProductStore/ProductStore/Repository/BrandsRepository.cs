﻿using ProductStore.Interfaces;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace ProductStore.Repository
{
    public class BrandsRepository : IDisposable, IBrandsRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        

        //getAll vraca listu brendova sortiranu po imenu brenda
        public IQueryable<Brand> GetAll()
        {
            return db.Brands.OrderBy(x=> x.Name);  
        }

        //getbyId vraca jedan element iz liste po zadatom parametru
        public Brand GetById(int id)
        {
            return db.Brands.FirstOrDefault(x=> x.Id == id);
        }

        //add prima objekat i dodaje listi
        public void Add(Brand brand)
        {
            db.Brands.Add(brand);
            db.SaveChanges();
        }

        //delete prima objekat i  brise iz liste
        public void Delete(Brand brand)
        {
            db.Brands.Remove(brand);
            db.SaveChanges();
        }

        //update primao objekat kao parametar, garantujemo da je objekat izmenjen, pronalazimo u bazi i pokusavamo da sacuvamo
        public void Update(Brand brand)
        {
            db.Entry(brand).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch(Exception e)
            {
                Logger.Logger.Log(e);
                // File.WriteAllText("../../Log/ExceptionLog.txt", "Dogodila se greska sa opisom: " + e.Message + ", molimo kontaktirajte programera");
            }
        }


        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}