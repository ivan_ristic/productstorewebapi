﻿using ProductStore.Interfaces;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ProductStore.Repository
{
    public class ProductRepository : IDisposable, IProductsRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private List<Product> productDb()
        {
            var dbContext = db.Set<Product>();
            dbContext.Include(x => x.Brand).Load();
            dbContext.Include(x => x.ProductCategory).Load();
            dbContext.Include(x => x.Gender).Load();
            var products = dbContext.OrderBy(x => x.Name).ToList();
            foreach (var product in products)
            {
                if (product.Brand != null)
                {
                    product.Brand = new Brand
                    {
                        Id = product.Brand.Id,
                        Name = product.Brand.Name
                    };
                }
            }

            foreach (var category in products)
            {
                if (category.ProductCategory != null)
                {
                    category.ProductCategory = new ProductCategory
                    {
                        Id = category.ProductCategory.Id,
                        Name = category.ProductCategory.Name
                    };
                }
            }

            foreach (var gender in products)
            {
                if (gender.Gender != null)
                {
                    gender.Gender = new Gender
                    {
                        Id = gender.Gender.Id,
                        Name = gender.Gender.Name
                    };
                }
            }
            return products;
        }

        //getall vraca iz baze listu proizvoda sortiranu po imenu
        public IQueryable<Product> GetAll()
        {

            return productDb().AsQueryable();
        }

        public IQueryable<Product> GetCheapFive()
        {
            
            return productDb().OrderBy(x => x.Price).Take(5).AsQueryable();

        }

        public IQueryable<Product> GetTopFive()
        {
           
            return productDb().OrderByDescending(x => x.Price).Take(5).AsQueryable();
        }

        public IQueryable<Product> GetByQuery(string query)
        {
            
            return productDb().Where(x => x.Name.ToLower().Contains(query.ToLower())).AsQueryable();

        }

        //getbyid prima parametar, trazimo u bazi i ukoliko nadjemo vracamo 
        public Product GetById(int id)
        {
            
            return productDb().FirstOrDefault(x => x.Id == id);
        }

        //add prima objekat kao parametar i ubacuje objekat u db
        public void Add(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
        }

        //delete prima objekat kao parametar, pronalazi ga i brise iz baze
        public void Delete(Product product)
        {
            db.Products.Remove(product);
            db.SaveChanges();
        }

        //update prima objekat, pokusava da primeni promenu u bazi i ukoliko dodje do greske poruku o gresci snimamo u fajlu
        public void Update(Product product)
        {
            db.Entry(product).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
                //File.WriteAllText("../../Log/ExceptionLog.txt", "Dogodila se greska sa opisom: " + e.Message + ", molimo kontaktirajte programera");
            }
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}