﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Unity;
using ProductStore.Interfaces;
using ProductStore.Repository;
using Unity.Lifetime;
using ProductStore.Resolver;
using ProductStore.Models;
using ProductStore.Interfaces.Services;
using ProductStore.App_Start;
using System.Web.Http.Cors;

namespace ProductStore
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.EnableCors(new EnableCorsAttribute("http://localhost:4200", headers: "*", methods: "*"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ////Dependency Injection
            //StructuremapWebApi.Start();
            //////Unity
            //var container = new UnityContainer();
            //container.RegisterType<IBrandsRepository, BrandsRepository>(new HierarchicalLifetimeManager());
            //container.RegisterType<IGendresRepository, GendersRepository>(new HierarchicalLifetimeManager());
            //container.RegisterType<IProductCategoriesRepository, ProductCategoriesRepository>(new HierarchicalLifetimeManager());
            //container.RegisterType<IProductsRepository, ProductRepository>(new HierarchicalLifetimeManager());
            ////container.RegisterType<IBrandsService<ApplicationDbContext, Brand>, Repository<>(new HierarchicalLifetimeManager());

            //config.DependencyResolver = new UnityResolver(container);
        }
    }
}
