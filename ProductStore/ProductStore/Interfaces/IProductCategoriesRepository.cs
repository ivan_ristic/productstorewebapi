﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Interfaces
{
    public interface IProductCategoriesRepository
    {
        //Metode koje su dostupne nad bazom
        IQueryable<ProductCategory> GetAll();
        ProductCategory GetById(int id);
        void Add(ProductCategory productCategory);
        void Update(ProductCategory productCategory);
        void Delete(ProductCategory productCategory);

    }
}
