﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Models;

namespace ProductStore.Interfaces
{
    public interface IRepository<in TDbContext, TEntity>
        where TDbContext : DbContext
        where TEntity : class
    {
        IQueryable<TEntity> GetAll(TDbContext dbContext);
        TEntity GetById(TDbContext dbContext, int key);
        void Insert(TDbContext dbContext, TEntity entity);
        void Delete(TDbContext dbContext, TEntity entity);
        void Update(TDbContext dbContext, TEntity entity);
        //void ClearAll(TDbContext dbContext);
        //void Delete(TDbContext dbContext, object key);
    }
}
