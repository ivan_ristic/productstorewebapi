﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Interfaces
{
   public interface IProductsRepository
    {
        //Metode koje su dostupne nad bazom
        IQueryable<Product> GetAll();
        IQueryable<Product> GetCheapFive();
        IQueryable<Product> GetTopFive();
        IQueryable<Product> GetByQuery(string query);
        Product GetById(int id);
        void Add(Product product);
        void Update(Product product);
        void Delete(Product product);
    }
}
