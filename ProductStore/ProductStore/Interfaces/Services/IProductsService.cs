﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Interfaces.Services
{
    public interface IProductsService
    {
        IQueryable<Product> GetAllProducts();
        Product GetProductById(int id);
        IQueryable<Product> GetbyQuery(string query);
        IQueryable<Product> GetByFilter(int min, int max);
        IQueryable<Product> GetCheapFive();
        IQueryable<Product> GetTopFive();
        void AddProduct(Product brand);
        void UpdateProduct(Product brand);
        void DeleteProduct(int id);
    }
}
