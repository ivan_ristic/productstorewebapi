﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Interfaces.Services
{
    public interface IGendersService
    {
        IQueryable<Gender> GetAllGenders();
        Gender GetGenderById(int id);
        void AddGender(Gender gender);
        void UpdateGender(Gender gender);
        void DeleteGender(int id);
    }
}
