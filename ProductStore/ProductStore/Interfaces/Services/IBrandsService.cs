﻿using ProductStore.Models;
using System.Linq;

namespace ProductStore.Interfaces.Services
{
    public interface IBrandsService
    {
        IQueryable<Brand> GetAllBrands();
        Brand GetBrandById(int id);
        void AddBrand(Brand brand);
        void UpdateBrand(Brand brand);
        void DeleteBrand(int id);
    }
}
