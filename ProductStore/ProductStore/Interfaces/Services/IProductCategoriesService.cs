﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Interfaces.Services
{
    public interface IProductCategoriesService
    {
        IQueryable<ProductCategory> GetAllCategories();
        ProductCategory GetCategoryById(int id);
        void AddCategory(ProductCategory category);
        void UpdateCategory(ProductCategory category);
        void DeleteCategory(int id);
    }
}
