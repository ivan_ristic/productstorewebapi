﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Interfaces
{
    public interface IGendresRepository
    {
        //Metode koje su dostupne nad bazom
        IQueryable<Gender> GetAll();
        Gender GetById(int id);
        void Add(Gender gendre);
        void Update(Gender gendre);
        void Delete(Gender gendre);
    }
}
