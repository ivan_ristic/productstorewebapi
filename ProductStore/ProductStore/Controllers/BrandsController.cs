﻿using ProductStore.Interfaces;
using ProductStore.Interfaces.Services;
using ProductStore.Models;
using ProductStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ProductStore.Controllers
{
    public class BrandsController : ApiController
    {
        private IBrandsService _service { get; set; }

        public BrandsController(IBrandsService service)
        {
            _service = service;
        }

        [ResponseType(typeof(Brand))]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult Get()
        {
            return Ok(_service.GetAllBrands());
        }

        [ResponseType(typeof(Brand))]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok(_service.GetBrandById(id));
        }

        [ResponseType(typeof(Brand))]
        [HttpPost]
        public IHttpActionResult Post(Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _service.AddBrand(brand);
            return Ok();
        }

        [ResponseType(typeof(Brand))]
        [HttpPut]
        public IHttpActionResult Put(int id, Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != brand.Id)
            {
                return BadRequest("Prosledjen Id i Id objekta se ne poklapaju");
            }
            _service.UpdateBrand(brand);
            return Ok();
        }

        [ResponseType(typeof(Brand))]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _service.DeleteBrand(id);
                return Ok();
            }
            catch(Exception e)
            {
                Logger.Logger.Log(e);
                return BadRequest(e.Message);
            }    
        }
    }
}
