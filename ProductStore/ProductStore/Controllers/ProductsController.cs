﻿using ProductStore.Interfaces.Services;
using ProductStore.Models;
using ProductStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ProductStore.Controllers
{
    public class ProductsController : ApiController
    {
        private IProductsService _service { get; set; }
        public ProductsController(IProductsService service)
        {
            _service = service;
        }

        [ResponseType(typeof(Product))]
        [HttpGet]
        public IHttpActionResult Get()
        {
            var products = _service.GetAllProducts();
            return Ok(products.ToList());
        }

        [ResponseType(typeof(Product))]
        [HttpGet]
        [Route("api/products/cheap5")]
        public IHttpActionResult CheapFive()
        {
            return Ok(_service.GetCheapFive().ToList());
        }

        [ResponseType(typeof(Product))]
        [HttpGet]
        [Route("api/products/top5")]
        public IHttpActionResult TopFive()
        {
            return Ok(_service.GetTopFive());
        }

        [ResponseType(typeof(Product))]
        [HttpGet]
        [Route("api/products/search")]
        public IHttpActionResult Get(string query)
        {
            return Ok(_service.GetbyQuery(query));
        }

        [ResponseType(typeof(Product))]
        [HttpGet]
        [Route("api/products/filter")]
        public IHttpActionResult Get(int min, int max)
        {
            return Ok(_service.GetByFilter(min, max));
        }

        [ResponseType(typeof(Product))]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok(_service.GetProductById(id));
        }

        [ResponseType(typeof(Product))]
        [HttpPost]
        public IHttpActionResult Post(Product product)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _service.AddProduct(product);
            return Ok();
        }

        [ResponseType(typeof(Product))]
        [HttpPut]
        public IHttpActionResult Put(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != product.Id)
            {
                return BadRequest("Prosledjen Id i Id objekta se ne poklapaju");
            }
            _service.UpdateProduct(product);
            return Ok();
        }

        [ResponseType(typeof(Product))]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _service.DeleteProduct(id);
                return Ok();
            }catch(Exception e)
            {
                Logger.Logger.Log(e);
                return BadRequest(e.Message);
            }  
        }
    }
}
