﻿using ProductStore.Interfaces.Services;
using ProductStore.Models;
using ProductStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ProductStore.Controllers
{
    public class GendersController : ApiController
    {
        private IGendersService _service { get; set; }

        public GendersController(IGendersService service)
        {
            _service = service;
        }

        [ResponseType(typeof(Gender))]
        [HttpGet]
        [AllowAnonymous]
        public IQueryable<Gender> Get()
        {
            return (_service.GetAllGenders());
        }

        [ResponseType(typeof(Gender))]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok(_service.GetGenderById(id));
        }

        [ResponseType(typeof(Gender))]
        [HttpPost]
        public IHttpActionResult Post(Gender gender)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _service.AddGender(gender);
            return Ok();
        }

        [ResponseType(typeof(Gender))]
        [HttpPut]
        public IHttpActionResult Put(int id, Gender gender)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != gender.Id)
            {
                return BadRequest("Prosledjen Id i Id objekta se ne poklapaju");
            }
            _service.UpdateGender(gender);
            return Ok();
        }

        [ResponseType(typeof(Gender))]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _service.DeleteGender(id);
                return Ok();
            }
            catch(Exception e)
            {
                Logger.Logger.Log(e);
                return BadRequest(e.Message);
            }  
        }
    }
}
