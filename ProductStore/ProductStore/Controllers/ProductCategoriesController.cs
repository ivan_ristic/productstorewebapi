﻿using ProductStore.Interfaces.Services;
using ProductStore.Models;
using ProductStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ProductStore.Controllers
{
    [AllowAnonymous]
    public class ProductCategoriesController : ApiController
    {
        private IProductCategoriesService _service { get; set; }

        public ProductCategoriesController(IProductCategoriesService service)
        {
            _service = service;
        }

        [ResponseType(typeof(ProductCategory))]
        [HttpGet]
        [AllowAnonymous]
        public IQueryable<ProductCategory> Get()
        {
            return _service.GetAllCategories();
        }

        [ResponseType(typeof(ProductCategory))]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok(_service.GetCategoryById(id));
        }

        [ResponseType(typeof(ProductCategory))]
        [HttpPost]
        public IHttpActionResult Post(ProductCategory category)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _service.AddCategory(category);
            return Ok();
        }

        [ResponseType(typeof(ProductCategory))]
        [HttpPut]
        public IHttpActionResult Put(int id, ProductCategory category)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != category.Id)
            {
                return BadRequest("Prosledjen Id i Id objekta se ne poklapaju");
            }
            _service.UpdateCategory(category);
            return Ok();
        }

        [ResponseType(typeof(ProductCategory))]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _service.DeleteCategory(id);
                return Ok();
            }
            catch(Exception e)
            {
                Logger.Logger.Log(e);
                return BadRequest(e.Message);
            }       
        }
    }
}
