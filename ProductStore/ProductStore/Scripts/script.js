﻿$(document).ready(function () {
    //podaci od interesa*************************************************************************
    var http = "http://";
    var host = window.location.host;
    var token = null;
    var headers = {};
    var productsEndPoint = "/api/products/";
    var categoryEndPoint = "/api/productcategories/";
    var brandsEndPoint = "/api/brands/";
    var gendersEndPoint = "/api/genders/";
    var cheapestFive = "/api/products/cheap5";
    var topFive = "/api/products/top5";
    var search = "/api/festivali/search";
    var filter = "/api/festivali/filter";

    var formAction = "Create";

    // priprema dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteProduct);
    // priprema dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editProduct);


    //Dugme za prikaz forme za registraciju ******************************************************
    $("#showReg").click(function (e) {
        e.preventDefault();
        $("#login").css('display', 'none');
        $("#showRegForm").css('display', 'block');
    });

    //Dugme za vracanje iz forme za registraciju u formu za login*********************************
    $("#backToLog").click(function (e) {
        e.preventDefault();
        $("#login").css('display', 'block');
        $("#showRegForm").css('display', 'none');
        refreshFormReg();
    });

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu**************************************************
    $("#logout").css("display", "none");


    //Dugme za logout ***************************************************************************************
    $("#btnLogout").click(function () {
        token = null;
        headers = {};

        $("#login").css("display", "block");
        $("#registration").css("display", "none");
        $("#logout").css("display", "none");
        $("#info").empty();
        $("#message").empty();
        $("#seed").empty();
        hideBlock("#btnAddNew");
        hideBlock("#refreshTable");
        showBlock("#registration");
        refreshFormLog();
    });

    // registracija korisnika************************************************************************************
    $("#registration").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var pass = $("#regPass").val();
        var pass2 = $("#regPass2").val();


        var ok = true;
        if (!email) {
            alert("Email je obavezno polje i mora sadrzati @.");
            ok = false;
        } else if (!pass) {
            alert("Sifra je obavezno polje i mora sadrzati minimum 6 karaktera, 1 veliko slovo i specijala karakter (,./ i slicno).");
            ok = false;
        } else if (!pass === pass2) {
            alert("Sifre se ne poklapaju.");
            ok = false;
        }if (!ok) {
                return;
            }

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": pass,
            "ConfirmPassword": pass2
        };
        $.ajax({
            type: "POST",
            url: http + host + "/api/Account/Register",
            data: sendData
        }).done(function (data) {
            $("#message").append("Uspešna registracija. Možete se prijaviti na sistem.");
            refreshFormReg();
            refreshFormLog();

        }).fail(function (data) {
            alert("Greska prilikom registracije");
        });
    });

    // prijava korisnika******************************************************************************************
    $("#login").submit(function (e) {
        e.preventDefault();

        var email = $("#logEmail").val();
        var pass = $("#logPass").val();

        //validacija ******
        //var ok = true;
        //if (!email) {
        //    alert("Email je obavezno polje.");
        //    ok = false;
        //} else if (!pass) {
        //    alert("Sifra je obavezno polje.");
        //    ok = false;
        //} if (!ok) {
        //    return;
        //}

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": pass
        };

        $.ajax({
            "type": "POST",
            "url": http + host + "/Token",
            "data": sendData
        }).done(function (data) {
            $("#info").empty().append(data.userName);
            token = data.access_token;
            $("#showRegForm").css("display", "none");
            $("#login").css("display", "none");
            showBlock("#logout");
            ////ucitavam tabelu
            loadProducts();
            $("#showAdd").css("display", "block")
            showBlock("#top5");
            showBlock("#cheap5");
            showBlock("#searchBox");
            showBlock("#refreshTable");
            showBlock("#formSearch");
        }).fail(function (data) {
            alert("Greska prilikom prijave");
        });
    });

    // odjava korisnika sa sistema***************************************************************************************
    $("#btnLogout").click(function () {
        token = null;
        headers = {};

        $("#showLoginForm").css("display", "block");
        hideBlock("#searchBox");
        hideBlock("#top5");
        hideBlock("#cheap5");
        hideBlock("#formProduct");
        hideBlock("#formSearch");

    });

    //prikaz registracije kada se klikne na button**********************************************************************
    $("#showReg").click(function (e) {
        e.preventDefault();
        hideBlock("#showLoginForm");
        showBlock("#showRegForm");
    });

    function loadProducts() {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        $.ajax({
            "type": "GET",
            "url": http + host + productsEndPoint,
            "headers": headers
        }).done(function (data) {
            setProducts(data, status);
            console.log(data, status);
        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });
    }

    //setovanje proizvoda u tabelu******************************************************************************************
    function setProducts(data, status) {
        console.log("Status: " + status);
        if (token) {
            $("#login").css("display", "none");
            $("#registration").css("display", "none");
        }
        var $container = $("#seed");
        $container.empty();

        if (data !== null) {

            // ispis naslova
            var div = $("<div></div>");
            var h1 = $("<h1>Products</h1>");
            div.append(h1);
            // ispis tabele
            var table = $("<table class='table table-bordered table-striped table-hover'></table>");
            var tbody = $("<tbody></tbody>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Price</td><td>Brand</td><td>Color</td><td>Size</td><td>Category</td><td>Gender</td><td>Delete</td><td>Edit</td></tr>");

            tbody.append(header);
            for (i = 0; i < data.length; i++) {
                // prikazujemo novi red u tabeli
                var row = "<tr>";
                // prikaz podataka
                var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td><td>" + data[i].Brand.Name + "</td><td>" + data[i].Color + "</td><td>" + data[i].Size + "</td><td>" + data[i].ProductCategory.Name + "</td><td>" + data[i].Gender.Name + "</td>";
                // prikaz dugmadi za izmenu i brisanje
                if (token) {
                    var stringId = data[i].Id.toString();
                    var displayDelete = "<td><button id=btnDelete name=" + stringId + " class='btn btn-danger'>Delete</button></td>";
                    var displayEdit = "<td><button id=btnEdit name=" + stringId + " class='btn btn-success'>Edit</button></td>";
                    row += displayData + displayDelete + displayEdit + "</tr>";
                    tbody.append(row);
                    newId = data[i].Id;
                }
                else {
                    row += displayData + "</tr>";
                    tbody.append(row);
                    newId = data[i].Id;
                }
            }
            table.append(tbody);
            div.append(table);
            div.append();
            // ispis novog sadrzaja
            $container.append(div);
        }
        else {
            div = $("<div></div>");
            h1 = $("<h1>Greška prilikom preuzimanja!</h1>");
            div.append(h1);
            $container.append(div);
        }
    }

    // brisanje *********************************************************************************************
    function deleteProduct() {
        // izvlacimo {id}
        console.log("delete");
        var deleteID = this.name;
        // saljemo zahtev 
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        $.ajax({
            url: http + host + productsEndPoint + deleteID.toString(),
            type: "DELETE",
            headers: headers
        })
            .done(function (data, status) {
                loadProducts();
            })
            .fail(function (data, status) {
                alert("Desila se greska prilikom brisanja!");
            });
    }

    //prikaz najjeftinijih proizvoda***************************************************************************
    $("#btnCheapest").click(function (e) {
        e.preventDefault();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        $.ajax({
            type: "GET",
            url: http + host + cheapestFive,
            headers: headers
        }).done(function (data) {
            setProducts(data, status);
            $("#reset").trigger("click");
            hideBlock("#formProduct");
        }).fail(function (data) {
            alert("Greska prilikom ucitavanja");
        });
    });

    //prikaz najskupljih proizvoda*************************************************************************************
    $("#btnTopFive").click(function (e) {
        e.preventDefault();
        
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        $.ajax({
            type: "GET",
            url: http + host + topFive,
            headers: headers
        }).done(function (data) {
            setProducts(data, status);
            $("#reset").trigger("click");
            hideBlock("#formProduct");
        }).fail(function (data) {
            alert("Greska prilikom ucitavanja");
        });
    });

    //Dugme za osvezavanje tabele **************************************************************************************
    $("#refreshT").click(function (e) {
        e.preventDefault();
        loadProducts();
        $("#reset").trigger("click");
        hideBlock("#formProduct");
        $("#min").val("");
        $("#max").val("");
        $("#searchInput").val("");
    })

    //Dugme za dodavanje ***********************************************************************************************
    $("#btnAddNew").click(function (e) {
        e.preventDefault();
        $("#showBrand").empty();
        var requestUrl = http + host + brandsEndPoint;
        $.getJSON(requestUrl, function (data, status) {
            $.each(data, function (i) {
                $("#showBrand").append($("<option></option>").val(data[i].Id).html(data[i].Name));
            });
        });

        //prikaz kategorije ************************************************************************************
        $("#showCategory").empty();
        var requestUrl = http + host + categoryEndPoint;
        $.getJSON(requestUrl, function (data, status) {
            $.each(data, function (i) {
                $("#showCategory").append($("<option></option>").val(data[i].Id).html(data[i].Name));
            });
        });

        // prikaz pola******************************************************************************************
        $("#showGender").empty();
        var requestUrl = http + host + gendersEndPoint;
        $.getJSON(requestUrl, function (data, status) {
            $.each(data, function (i) {
                $("#showGender").append($("<option></option>").val(data[i].Id).html(data[i].Name));
            });
        });
        showBlock("#formProduct");
    });

    //Editovanje proizvoda ************************************************************************************
    function editProduct() {
        // izvlacimo id
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        showBlock("#formProduct");

        $("#showBrand").empty();
        var requestUrl = http + host + brandsEndPoint;
        $.getJSON(requestUrl, function (data, status) {
            $.each(data, function (i) {
                $("#showBrand").append($("<option></option>").val(data[i].Id).html(data[i].Name));
            });
        });

        $("#showCategory").empty();
        var requestUrl = http + host + categoryEndPoint;
        $.getJSON(requestUrl, function (data, status) {
            $.each(data, function (i) {
                $("#showCategory").append($("<option></option>").val(data[i].Id).html(data[i].Name));
            });
        });

        $("#showGender").empty();
        var requestUrl = http + host + gendersEndPoint;
        $.getJSON(requestUrl, function (data, status) {
            $.each(data, function (i) {
                $("#showGender").append($("<option></option>").val(data[i].Id).html(data[i].Name));
            });
        });

        //Odnosi se na name html elementa koji je pozvao funkciju edit
        var editId = this.name;
        $.ajax({
            "url": http + host + productsEndPoint + editId.toString(),
            "type": "GET",
            "headers": headers
        })
            .done(function (data) {
                $("#productName").val(data.Name);
                $("#productColor").val(data.Color);
                $("#productPrice").val(data.Price);
                $("#productSize").val(data.Size);
                $("#showBrand").val(data.Brand.Id);
                $("#showGender").val(data.Gender.Id);
                $("#showCategory").val(data.ProductCategory.Id);

                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data) {
                formAction = "Create";
                alert("Desila se greska!");
            });
    }

    // dodavanje novog proizvoda************************************************************************************
    $("#formProduct").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        var name = $("#productName").val();
        var color = $("#productColor").val();
        var price = $("#productPrice").val();
        var size = $("#productSize").val();
        var brand = $('#showBrand').val();
        var category = $('#showCategory').val();
        var gender = $('#showGender').val();


        //***validacija *****
        var ok = true;
        if (!name) {
            alert("Ime je obavezno polje.");
            ok = false;
        } else if (name.length > 90) {
            alert("Ime ne moze imati vise od 90 karaktera.");
            ok = false;
        } else if (!color) {
            alert("Boja je obavezno polje.");
            ok = false;
        } else if (color.length > 40) {
            alert("Boja ne sme imati vise od 40 karaktera.");
            ok = false;
        } else if (!price) {
            alert("Cena je obavezno polje i mora biti pozitivan broj.");
            ok = false;
        }else if (!size) {
            alert("Velicina je obavezno polje");
            ok = false;
        if (!ok)
            return;
        }


            var httpAction;
            var sendData;
            var url;

            if (token) {
                headers.Authorization = 'Bearer ' + token;
            }
            // u zavisnosti od akcije pripremam objekat
            if (formAction === "Create") {
                httpAction = "POST";
                url = http + host + productsEndPoint;

                sendData = {
                    "Name": name,
                    "Size": size,
                    "Color": color,
                    "Price": price,
                    "BrandId": brand,
                    "GenderId": gender,
                    "ProductCategoryId": category
                };
            }
            else {
                httpAction = "PUT";
                url = http + host + productsEndPoint + editingId.toString();

                sendData = {
                    "Id": editingId,
                    "Name": name,
                    "Color": color,
                    "Size": size,
                    "Price": price,
                    "BrandId": brand,
                    "GenderId": gender,
                    "ProductCategoryId": category
                };
            }

            console.log("Objekat za slanje");
            console.log(sendData);

            $.ajax({
                url: url,
                type: httpAction,
                data: sendData,
                headers: headers
            })
                .done(function (data, status) {
                    formAction = "Create";
                    loadProducts();
                    //refreshFormProduct();
                })
                .fail(function (data, status) {
                    alert("Desila se greska!");
                });
        });

    //Pretraga proizvoda *****************************************************************************************
    $("#btnSearch").click(function (e) {
        e.preventDefault();
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        console.log("searching");
        var query = $("#searchInput").val();
        // saljemo zahtev na getproductsbycategory
        $.ajax({
            "url": http + host + "/api/products/search?query=" + query.toString(),
            "type": "GET",
            "headers": headers
        })
		.done(function (data, status) {
		    setProducts(data, status);
		})
		.fail(function (data) {
		    alert("Desila se greska prilikom pretrage!");
		});
    });


    //Filtriranje proizvoda po ceni **********************
    $("#filterMinMax").click(function (e) {
        e.preventDefault();
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        console.log("filter");
        var min = $("#min").val();
        var max = $("#max").val();


        if (!Number.isInteger(min) && Number.isInteger(max)) {
            alert("Obavezna su oba polja i morate uneti pozitivne vrednosti.");
            return;
        }

        // saljemo zahtev na getproductsbycategory
        $.ajax({
            "url": http + host + "/api/products/filter?min=" + min.toString() + "&max=" + max.toString(),
            "type": "GET",
            "headers": headers
        })
		.done(function (data, status) {
		    setProducts(data, status);
		    $("#min").val("");
		    $("#max").val("");
		})
		.fail(function (data) {
		    alert("Desila se greska prilikom filtracije!");
		});
    });


    //taster za resetovanje forme *************************************************************************************
    $("#btnReset").click(function () {
        refreshProductTable();
    });

    //funkcija za prikaz elementa *************************************************************************************
    function showBlock(elementId) {
        $(elementId).css("display", "block");
    }

    //funkcija za sakrivanje elementa ********************************************************************************
    function hideBlock(elementId) {
        $(elementId).css("display", "none");
    }

    //Funkcija za ciscenje polja iz forme za registraciju ***********************************************************
    function refreshFormReg() {
        // cistim formu
        $("#regEmail").val('');
        $("#regPass").val('');
        $("#regPass2").val('');
    }

    //Funkcija za ciscenje polja iz forme za log in *****************************************************************
    function refreshFormLog() {
        // cistim formu
        $("#logEmail").val('');
        $("#logPass").val('');
    }
});