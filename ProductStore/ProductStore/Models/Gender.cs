﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductStore.Models
{
    public class Gender
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Pol je obavezno polje i ne moze biti duze od 50 karaktera.")]
        [StringLength(50)]
        public string Name { get; set; }
    }
}