﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductStore.Models
{
    public class ProductCategory
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Ime je obavezno polje i ne moze biti duze od 60 karaktera.")]
        [StringLength(60)]
        public string Name { get; set; }
        public List<Product> Products { get; set; }
    }
}