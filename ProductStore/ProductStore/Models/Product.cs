﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductStore.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Ime je obavezno polje.")]
        [StringLength(90)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Cena je obavezno polje i mora biti pozitivan broj.")]
        [Range(0, double.MaxValue)]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Boja je obavezno polje i ne moze biti duze od 40 karaktera.")]
        [StringLength(40)]
        public string Color { get; set; }

        [Required(ErrorMessage = "Velicina je obavezno polje i ne moze biti duze od 10 karaktera.")]
        [StringLength(10)]
        public string Size { get; set; }

        public int GenderId { get; set; }
        public Gender Gender { get; set; }

        public int ProductCategoryId { get; set; }
        [Display(Name = "Category")]
        public ProductCategory ProductCategory { get; set; }

        public int BrandId { get; set; }
        public Brand Brand { get; set; }
    }
}