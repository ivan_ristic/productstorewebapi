﻿using ProductStore.Interfaces;
using ProductStore.Interfaces.Services;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductStore.Services
{
    public class GendersService : IGendersService
    {
        private readonly IRepository<ApplicationDbContext, Gender> repo;
        private readonly ApplicationDbContext _db;
        //Servis je za komunikaciju izmedju kontrolera i repositorija
        public GendersService(IApplicationDbContext db, IRepository<ApplicationDbContext, Gender> repository)
        {
            repo = repository;
            _db = (ApplicationDbContext)db;
        }

        //Servis GetAll sluzi za prikaz svih polova
        public IQueryable<Gender> GetAllGenders()
        {
            return repo.GetAll(_db);
        }

        //Servis GetGendreById sluzi za prikaz pola po id-u, ulazni parametar je id
        public Gender GetGenderById(int id)
        {
            return repo.GetById(_db, id);
        }

        //Sevis AddGendre sluzi za dodavanje polova, ulazni parametar je objekat
        public void AddGender(Gender gender)
        {
            repo.Insert(_db, gender);
            _db.SaveChanges();
        }

        //Servis EditGendre sluzi za editovanje pola, ulazni parametar je objekat
        public void UpdateGender(Gender gender)
        {
            try
            {
                repo.Update(_db, gender);
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
            }
            _db.SaveChanges();
        }

        //Servis DeleteGendre sluzi za brisanje pola, ulazni parametar je id
        public void DeleteGender(int id)
        {
            var gender = repo.GetById(_db, id);
            if (gender == null)
            {
                throw new Exception("Nije pronadjen pol");
            }
            repo.Delete(_db, gender);
            _db.SaveChanges();
        }
    }
}