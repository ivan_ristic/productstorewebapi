﻿using ProductStore.Interfaces;
using ProductStore.Interfaces.Services;
using ProductStore.Models;
using System;
using System.Linq;

namespace ProductStore.Services
{
    public class BrandsService : IBrandsService
    {
        //Servis je za komunikaciju izmedju kontrolera i repositorija
        private readonly IRepository<ApplicationDbContext, Brand> repo;
        private readonly ApplicationDbContext _db;

        public BrandsService(IApplicationDbContext db, IRepository<ApplicationDbContext, Brand> repository)
        {
            repo = repository;
            _db = (ApplicationDbContext)db;
        }

        //Servis GetAllBrands vraca iz repozitorijuma sve brendove
        public IQueryable<Brand> GetAllBrands()
        {
            return repo.GetAll(_db);
        }

        //Servis GetById vraca brend po id-u 
        public Brand GetBrandById(int id)
        {
            var brand = repo.GetById(_db, id);
            if (brand == null)
            {
                throw new Exception("Nije pronadjen brend");
            }
            return (brand);
        }

        //Servis AddBrand sluzi za dodavanje, ulazni parametar je objekat 
        public void AddBrand(Brand brand)
        {
            repo.Insert(_db, brand);
            _db.SaveChanges();
        }

        //Servis EditBrand sluzi za editovanje, ulazni parametar je objekat
        public void UpdateBrand(Brand brand)
        {
            try
            {
                repo.Update(_db, brand);
                
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
            }
            _db.SaveChanges();
        }

        //Servis DeleteBrand sluzi za brisanje, ulazni parametar je Id
        public void DeleteBrand(int id)
        {
            var brand = repo.GetById(_db, id);
            if (brand == null)
            {
                throw new Exception("Nije pronadjen brend");
            }
            repo.Delete(_db, brand);
            _db.SaveChanges();
        }
    }
}