﻿using ProductStore.Interfaces;
using ProductStore.Interfaces.Services;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductStore.Services
{
    public class ProductCategoriesService : IProductCategoriesService
    {
        //Servis je za komunikaciju izmedju kontrolera i repositorija
        private readonly IRepository<ApplicationDbContext, ProductCategory> repo;
        private readonly ApplicationDbContext _db;

        public ProductCategoriesService(IApplicationDbContext db, IRepository<ApplicationDbContext, ProductCategory> repository)
        {
            repo = repository;
            _db = (ApplicationDbContext)db;
        }

        //Servis GetAllCategories sluzi za prikaz svih katerogija
        public IQueryable<ProductCategory> GetAllCategories()
        {
            return repo.GetAll(_db);
        }

        //Servis GetcategoryById sluzi za prikaz kategorije po id-u
        public ProductCategory GetCategoryById(int id)
        {
            return repo.GetById(_db, id);
        }

        //Servis AddCategory sluzi za dodavanje kategorije, ulazni parametar je objekat
        public void AddCategory(ProductCategory category)
        {
            repo.Insert(_db, category);
            _db.SaveChanges();
        }

        //Servis EditCategory sluzi za editovanje katergoije, ulazni parametar je objekat
        public void UpdateCategory (ProductCategory category)
        {
            try
            {
                repo.Update(_db, category);
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
            }
            _db.SaveChanges();
        }

        //Servis DeleteCategory sluzi za brisanje kategije, ulazni parametar je id
        public void DeleteCategory(int id)
        {
            var category = repo.GetById(_db, id);
            if (category == null)
            {
                throw new Exception("Nije pronadjena kategorija");
            }
            repo.Delete(_db, category);
            _db.SaveChanges();
        }
    }
}