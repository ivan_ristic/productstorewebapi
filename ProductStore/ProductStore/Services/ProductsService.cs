﻿using ProductStore.Interfaces;
using ProductStore.Interfaces.Services;
using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ProductStore.Services
{
    public class ProductsService : IProductsService
    {
        //Servis je za komunikaciju izmedju kontrolera i repositorija

        private readonly ApplicationDbContext _db;
        private IRepository<ApplicationDbContext, Product> repo;

        public ProductsService(IApplicationDbContext db, IRepository<ApplicationDbContext, Product> repository)
        {
            repo = repository;
            _db = (ApplicationDbContext)db;
        }

        //Servis GetAllProducts sluzi za prikaz svih proizvoda
        public IQueryable<Product> GetAllProducts()
        {
            var result = productsDb().AsQueryable();
            //var products = repo.GetAll(_db);
            
            return result;
        }

        public IQueryable<Product> GetCheapFive()
        {
            var result = productsDb().OrderBy(x => x.Price).Take(5).Skip(0).AsQueryable();
            //return repo.GetAll(_db).OrderBy(x=> x.Price).Take(5).Skip(0);
            return result;
        }

        public IQueryable<Product> GetTopFive()
        {
            var result = productsDb().OrderByDescending(x => x.Price).Take(5).Skip(0).AsQueryable();
            //return repo.GetAll(_db).OrderByDescending(x => x.Price).Take(5).Skip(0);
            return result;
        }

        public IQueryable<Product> GetbyQuery(string query)
        {
            if(query == null)
            {
                return productsDb().AsQueryable();
            }
            else
            {
                var result = productsDb().Where(x=> x.Name.ToLower().Contains(query.ToLower())).AsQueryable();
                //var result = repo.GetAll(_db).Where(x => x.Name.ToLower().Contains(query.ToLower()));
                return result;
            } 
        }

        public IQueryable<Product> GetByFilter(int min, int max)
        {
            if (min == 0 || max == 0)
            {
                return productsDb().AsQueryable();
            }
            var result = productsDb().Where(x => x.Price >= min && x.Price <= max).AsQueryable();
            return result;
        }

        //Servis GetProductsById sluzi za prikaz proizvoda po id-u
        public Product GetProductById(int id)
        {
            var result = productsDb().FirstOrDefault(x => x.Id == id);
            //return repo.GetById(_db, id);
            return result;
        }

        //Servis AddProduct sluzi za dodavanje prozivoda, ulazni parametar je objekat
        public void AddProduct(Product product)
        {
            repo.Insert(_db, product);
            _db.SaveChanges();
        }

        //Servis EditProduct sluzi za editovanje prozivoda, ulazni parametar je objekat
        public void UpdateProduct(Product product)
        {
            try
            {
                repo.Update(_db, product);
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Logger.Log(e);
            }
        }

        //Servis DeleteProduct sluzi za brisanje proizvoda, ulazni parametar je id
        public void DeleteProduct(int id)
        {
            var product = repo.GetById(_db, id);
            if (product == null)
            {
                throw new Exception("Nije pronadjen proizvod");
            }
            repo.Delete(_db, product);
            _db.SaveChanges();
        }

        private List<Product> productsDb()
        {
            var dbContext = _db.Set<Product>();
            dbContext.Include(x => x.Brand).Load();
            dbContext.Include(x => x.ProductCategory).Load();
            dbContext.Include(x => x.Gender).Load();
            var products = dbContext.OrderBy(x => x.Name).ToList();
            foreach (var product in products)
            {
                if (product.Brand != null)
                {
                    product.Brand = new Brand
                    {
                        Id = product.Brand.Id,
                        Name = product.Brand.Name
                    };
                }
            }

            foreach (var category in products)
            {
                if (category.ProductCategory != null)
                {
                    category.ProductCategory = new ProductCategory
                    {
                        Id = category.ProductCategory.Id,
                        Name = category.ProductCategory.Name
                    };
                }
            }

            foreach (var gender in products)
            {
                if (gender.Gender != null)
                {
                    gender.Gender = new Gender
                    {
                        Id = gender.Gender.Id,
                        Name = gender.Gender.Name
                    };
                }
            }
            return products;
        }
    }
}