namespace ProductStore.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductStore.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //adding new genres in db
            context.Genders.AddOrUpdate(x => x.Id,
                new Gender { Id = 1, Name = "Male" },
                new Gender { Id = 2, Name = "Female" });
            //adding new brands in db
            context.Brands.AddOrUpdate(x => x.Id,
                new Brand { Id = 1, Name = "Nike" },
                new Brand { Id = 2, Name = "Reebok" },
                new Brand { Id = 3, Name = "Puma" },
                new Brand { Id = 4, Name = "Levis" },
                new Brand { Id = 5, Name = "Zara" });
            //adding new Categories in db
            context.ProductCategories.AddOrUpdate(x => x.Id,
                new ProductCategory { Id = 1, Name = "Clothes" },
                new ProductCategory { Id = 2, Name = "Shoes" },
                new ProductCategory { Id = 3, Name = "Accessories" });
            //adding new products in db
            context.Products.AddOrUpdate(x => x.Id,
                new Product { Id = 1, Name = "Jacket", Size = "XL", Color = "Blue", Price = 4999, BrandId = 1, GenderId = 1, ProductCategoryId = 1 },
                new Product { Id = 2, Name = "Jeans", Size = "S", Color = "Black", Price = 3999, BrandId = 4, GenderId = 2, ProductCategoryId = 1 },
                new Product { Id = 3, Name = "T-Shirt", Size = "M", Color = "White", Price = 1200, BrandId = 3, GenderId = 1 , ProductCategoryId = 1},
                new Product { Id = 4, Name = "Shirt", Size = "XS", Color = "Light blue", Price = 2500, BrandId = 4, GenderId = 2 , ProductCategoryId = 1},
                new Product { Id = 5, Name = "Coat", Size = "XL", Color = "Blue", Price = 4999, BrandId = 5, GenderId = 2 , ProductCategoryId = 1},
                new Product { Id = 6, Name = "Boots", Size = "42", Color = "Brown", Price = 8900, BrandId = 1, GenderId = 2 , ProductCategoryId = 2},
                new Product { Id = 7, Name = "Sneakers", Size = "43", Color = "White", Price = 6900, BrandId = 2, GenderId = 1 , ProductCategoryId = 2},
                new Product { Id = 8, Name = "Sunglasses", Size = "Universal", Color = "Black", Price = 700, BrandId = 5, GenderId = 1, ProductCategoryId = 3 });
            
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
