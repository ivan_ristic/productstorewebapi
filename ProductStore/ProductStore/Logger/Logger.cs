﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProductStore.Logger
{
    public class Logger
    {
        //Definisanje putanje log fajla
        public static string path = "../../Log/";

        //Provera da li putanja postoji
        public static void VerifyDir(string path)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.Exists)
                {
                    dir.Create();
                }
            }
            catch (Exception e)
            {
                File.WriteAllText("../../Log/ExceptionLog.txt", "Dogodila se greska sa opisom: " + e.Message + ", molimo kontaktirajte programera");
            }
        }


        public static void Log(Exception e)
        {
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "_Logs.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine("[" + DateTime.Now.ToString() + "]" + " Exception is handled and this is message about that specific exception " + e.Message);
                file.Close();
            }
            catch (Exception ex)
            {
                File.WriteAllText("../../Log/ExceptionLog.txt", "Dogodila se greska sa opisom: " + ex.Message + ", molimo kontaktirajte programera");
            }
        }
    }

}